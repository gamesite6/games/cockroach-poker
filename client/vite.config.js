import { defineConfig } from "vite";
import { svelte } from "@sveltejs/vite-plugin-svelte";

// https://vitejs.dev/config/
export default defineConfig({
  base: "/static/beetle-bluff/",
  build: {
    emptyOutDir: false,
    lib: {
      entry: "src/index.ts",
      fileName: "beetle-bluff",
      name: "Gamesite6_BeetleBluff",
    },
  },
  plugins: [svelte()],
});
