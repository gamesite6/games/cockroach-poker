type PlayerId = IPlayerId;

type IPlayerId = number;

type GameSettings = IGameSettings;

interface IGameSettings {
  selectedPlayerCounts: number[];
}

type GameState = IGameState;

interface IGameState {
  players: Player[];
  phase: Phase;
}

type Player = IPlayer;

interface IPlayer {
  id: PlayerId;
  hand: Card[];
  palmed: Card[];
}

type Card = ICard;

type ICard = number;

type Suit = "A" | "B" | "C" | "D" | "E" | "F" | "G" | "H";

type Action =
  | ["Give", IGive]
  | ["Peek", IPeek]
  | ["Pass", IPass]
  | ["Accept", IAccept]
  | ["Ready", IReady];

type IGive = GiveData;

type IPeek = void[];

type IPass = PassData;

type IAccept = AcceptData;

type IReady = void[];

type GiveData = IGiveData;

interface IGiveData {
  actual: Card;
  claimed: Suit;
  receiver: PlayerId;
}

type PassData = IPassData;

interface IPassData {
  claimed: Suit;
  receiver: PlayerId;
}

type AcceptData = IAcceptData;

interface IAcceptData {
  belief: boolean;
}

type Phase =
  | ["Giving", IGiving]
  | ["Receiving", IReceiving]
  | ["Acceptance", IAcceptance]
  | ["Passing", IPassing]
  | ["GameComplete", IGameComplete];

type IGiving = GivingData;

type IReceiving = ReceivingData;

type IAcceptance = AcceptanceData;

type IPassing = PassingData;

type IGameComplete = GameCompleteData;

type GivingData = IGivingData;

interface IGivingData {
  giver: PlayerId;
}

type ReceivingData = IReceivingData;

interface IReceivingData {
  claim: Claim;
  actual: Card;
  previous: Claim[];
}

type AcceptanceData = IAcceptanceData;

interface IAcceptanceData {
  claim: Claim;
  belief: boolean;
  actual: Card;
  ready: PlayerId[];
}

type PassingData = IPassingData;

interface IPassingData {
  giver: PlayerId;
  card: Card;
  claims: Claim[];
}

type GameCompleteData = IGameCompleteData;

interface IGameCompleteData {
  loser: PlayerId;
}

type Claim = IClaim;

interface IClaim {
  giver: PlayerId;
  receiver: PlayerId;
  suit: Suit;
}
