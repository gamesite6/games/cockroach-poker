import { sortBy } from "lodash-es";

export function rotateUserFirst(allPlayers: Player[], playerId: PlayerId) {
  let playerIndex = allPlayers.findIndex((player) => player.id === playerId);

  if (playerIndex === -1) {
    return allPlayers;
  } else {
    return [
      ...allPlayers.slice(playerIndex),
      ...allPlayers.slice(0, playerIndex),
    ];
  }
}

export function rotateUserLast(allPlayers: Player[], playerId: PlayerId) {
  let playerIndex = allPlayers.findIndex((player) => player.id === playerId);

  if (playerIndex === -1) {
    return allPlayers;
  } else {
    return [
      ...allPlayers.slice(playerIndex + 1),
      ...allPlayers.slice(0, playerIndex + 1),
    ];
  }
}

export function isActivePlayer(state: GameState, playerId: PlayerId): boolean {
  switch (state.phase[0]) {
    case "Giving": {
      return state.phase[1].giver === playerId;
    }
    case "Receiving": {
      return state.phase[1].claim.receiver === playerId;
    }
    case "Acceptance": {
      return (
        (state.phase[1].claim.giver === playerId ||
          state.phase[1].claim.receiver === playerId) &&
        !state.phase[1].ready.includes(playerId)
      );
    }
    case "Passing": {
      return state.phase[1].giver === playerId;
    }
    case "GameComplete":
      return false;
  }
}

export const suits: Suit[] = ["A", "B", "C", "D", "E", "F", "G", "H"];

const CARDS_PER_SUIT = 8;

export function cardSuit(card: Card): Suit {
  const suitIndex = Math.floor((card - 1) / CARDS_PER_SUIT) % suits.length;
  return suits[suitIndex];
}

export function getClaims(state: GameState): IClaim[] {
  switch (state.phase[0]) {
    case "Giving": {
      return [];
    }
    case "Receiving": {
      const { claim, previous } = state.phase[1];
      return [claim, ...previous];
    }
    case "Acceptance": {
      return [];
    }
    case "Passing": {
      return state.phase[1].claims;
    }
    case "GameComplete": {
      return [];
    }
  }
}

export function getSuitCounts(cards: Card[]): Map<Suit, number> {
  const counts = new Map<Suit, number>();

  for (const card of cards) {
    const suit = cardSuit(card);
    const previousCount = counts.get(suit) ?? 0;
    counts.set(suit, previousCount + 1);
  }

  return counts;
}

export function groupBySuits(cards: Card[]): Card[][] {
  const groups = new Map<Suit, Card[]>();

  for (const card of cards) {
    const suit = cardSuit(card);

    const group = groups.get(suit) ?? [];
    groups.set(suit, group);

    group.push(card);
  }

  return sortBy(
    sortBy(Array.from(groups.values()), (group) => cardSuit(group[0])),
    (group) => -group.length
  );
}
