import Shared.GameState.Card (cardSuit)
import qualified Shared.GameState.Card as Card
import Test.Tasty
import Test.Tasty.HUnit

main :: IO ()
main = defaultMain tests

tests :: TestTree
tests = testGroup "Tests" [cardTests]

cardTests =
  testGroup
    "Card"
    [ testCase "all cards have correct suit" $
        (cardSuit <$> Card.allCards)
          @?= concat
            [ replicate 8 Card.A,
              replicate 8 Card.B,
              replicate 8 Card.C,
              replicate 8 Card.D,
              replicate 8 Card.E,
              replicate 8 Card.F,
              replicate 8 Card.G,
              replicate 8 Card.H
            ]
    ]