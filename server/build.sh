#!/bin/bash
stack clean
stack build --ghc-options -O2 --pedantic --copy-bins --local-bin-path $(pwd)/target
