module Shared.GameState.Phase.Giving where

import Shared.PlayerId

data GivingData = GivingData
  { giver :: PlayerId
  }
  deriving (Eq, Show)
