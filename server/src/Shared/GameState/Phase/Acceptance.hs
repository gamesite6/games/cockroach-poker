module Shared.GameState.Phase.Acceptance where

import Data.Set (Set)
import Shared.GameState.Card
import Shared.GameState.Phase.Claim
import Shared.PlayerId

data AcceptanceData = AcceptanceData
  { claim :: Claim,
    belief :: Bool,
    actual :: Card,
    ready :: Set PlayerId
  }
  deriving (Eq, Show)