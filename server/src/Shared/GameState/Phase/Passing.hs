module Shared.GameState.Phase.Passing where

import Shared.GameState.Card
import Shared.GameState.Phase.Claim
import Shared.PlayerId

data PassingData = PassingData
  { giver :: PlayerId,
    card :: Card,
    claims :: [Claim]
  }
  deriving (Eq, Show)
