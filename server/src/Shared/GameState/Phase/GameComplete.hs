module Shared.GameState.Phase.GameComplete where

import Shared.PlayerId

data GameCompleteData = GameCompleteData
  { loser :: PlayerId
  }
  deriving (Eq, Show)
