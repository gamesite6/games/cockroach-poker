module Shared.GameState.Phase.Receiving where

import Shared.GameState.Card
import Shared.GameState.Phase.Claim

data ReceivingData = ReceivingData
  { claim :: Claim,
    actual :: Card,
    previous :: [Claim]
  }
  deriving (Eq, Show)
