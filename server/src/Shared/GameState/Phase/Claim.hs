module Shared.GameState.Phase.Claim where

import Shared.GameState.Card
import Shared.PlayerId

type Giver = PlayerId

type Receiver = PlayerId

data Claim = Claim
  { giver :: PlayerId,
    receiver :: PlayerId,
    suit :: Suit
  }
  deriving (Eq, Show)