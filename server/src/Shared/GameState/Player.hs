module Shared.GameState.Player where

import Shared.GameState.Card
import Shared.PlayerId

data Player = Player
  { id :: PlayerId,
    hand :: [Card], -- cards in this player's hand
    palmed :: [Card] -- cards that have been palmed off on this player
  }
  deriving (Eq, Show)
