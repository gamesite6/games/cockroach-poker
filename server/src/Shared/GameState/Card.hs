module Shared.GameState.Card where

newtype Card = Card Int deriving (Eq, Show)

cardsPerSuit :: Int
cardsPerSuit = 8

allCards :: [Card]
allCards = Card <$> [1 .. cardsPerSuit * length allSuits]

cardSuit :: Card -> Suit
cardSuit (Card card) =
  let suitIdx = ((card - 1) `div` cardsPerSuit) `mod` length allSuits
   in allSuits !! suitIdx

data Suit
  = A
  | B
  | C
  | D
  | E
  | F
  | G
  | H
  deriving (Enum, Bounded, Eq, Ord, Show)

allSuits :: [Suit]
allSuits = [minBound .. maxBound]