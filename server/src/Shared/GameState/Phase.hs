module Shared.GameState.Phase where

import Shared.GameState.Phase.Acceptance
import Shared.GameState.Phase.GameComplete
import Shared.GameState.Phase.Giving
import Shared.GameState.Phase.Passing
import Shared.GameState.Phase.Receiving

data Phase
  = Giving GivingData
  | Receiving ReceivingData
  | Acceptance AcceptanceData
  | Passing PassingData
  | GameComplete GameCompleteData
  deriving (Eq, Show)
