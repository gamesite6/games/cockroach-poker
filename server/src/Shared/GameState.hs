{-# LANGUAGE NamedFieldPuns #-}

module Shared.GameState where

import Shared.GameState.Phase
import Shared.GameState.Player

data GameState = GameState
  { players :: [Player],
    phase :: Phase
  }
  deriving (Eq, Show)

isCompleted :: GameState -> Bool
isCompleted GameState {phase} = case phase of
  GameComplete _ -> True
  _ -> False
