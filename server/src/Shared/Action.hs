module Shared.Action where

import Shared.Action.Accept
import Shared.Action.Give
import Shared.Action.Pass

data Action
  = Give GiveData
  | Peek
  | Pass PassData
  | Accept AcceptData
  | Ready
  deriving (Eq, Show)
