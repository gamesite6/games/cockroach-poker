module Shared.Action.Accept where

data AcceptData = AcceptData
  { belief :: Bool
  }
  deriving (Eq, Show)