module Shared.Action.Give where

import Shared.GameState.Card
import Shared.PlayerId

data GiveData = GiveData
  { actual :: Card,
    claimed :: Suit,
    receiver :: PlayerId
  }
  deriving (Eq, Show)
