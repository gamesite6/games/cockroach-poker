module Shared.Action.Pass where

import Shared.GameState.Card
import Shared.PlayerId

data PassData = PassData
  { claimed :: Suit,
    receiver :: PlayerId
  }
  deriving (Eq, Show)
