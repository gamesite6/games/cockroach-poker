{-# LANGUAGE NamedFieldPuns #-}

module Shared.Settings where

newtype Seed = Seed {unSeed :: Int}

newtype GameSettings = GameSettings
  { selectedPlayerCounts :: [Int]
  }
  deriving (Eq, Show)

defaultSettings :: GameSettings
defaultSettings = GameSettings [3, 4, 5, 6]

validPlayerCounts :: [Int]
validPlayerCounts = [2, 3, 4, 5, 6]

validatePlayerCounts :: GameSettings -> [Int]
validatePlayerCounts GameSettings {selectedPlayerCounts} = filter (`elem` selectedPlayerCounts) validPlayerCounts
