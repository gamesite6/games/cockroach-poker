module Dto.InfoReq where

import Shared.Settings (GameSettings)

newtype InfoReq = InfoReq
  { settings :: GameSettings
  }
  deriving (Eq, Show)
