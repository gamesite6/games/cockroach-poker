module Dto.InitialStateRes where

import Shared.GameState

data InitialStateRes = InitialStateRes
  { state :: GameState
  }
  deriving (Eq, Show)
