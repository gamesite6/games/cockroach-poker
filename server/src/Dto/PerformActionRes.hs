module Dto.PerformActionRes where

import Shared.GameState

data PerformActionRes = PerformActionRes
  { completed :: Bool,
    nextState :: GameState
  }
  deriving (Eq, Show)