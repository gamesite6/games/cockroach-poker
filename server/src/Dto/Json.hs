{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}
{-# OPTIONS_GHC -fno-warn-orphans #-}

module Dto.Json where

import Data.Aeson
import Data.Aeson.TypeScript.TH
import Dto.InfoReq
import Dto.InfoRes
import Dto.InitialStateReq
import Dto.InitialStateRes
import Dto.PerformActionReq
import Dto.PerformActionRes
import Shared.Action
import Shared.Action.Accept
import Shared.Action.Give
import Shared.Action.Pass
import Shared.GameState
import Shared.GameState.Card
import Shared.GameState.Phase
import Shared.GameState.Phase.Acceptance
import Shared.GameState.Phase.Claim
import Shared.GameState.Phase.GameComplete
import Shared.GameState.Phase.Giving
import Shared.GameState.Phase.Passing
import Shared.GameState.Phase.Receiving
import Shared.GameState.Player
import Shared.PlayerId
import Shared.Settings

$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''PlayerId)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''GameSettings)

$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Action)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''GiveData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''PassData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''AcceptData)

$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Phase)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''GivingData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''ReceivingData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''AcceptanceData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''PassingData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''GameCompleteData)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Claim)

$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''GameState)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Player)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Card)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''Suit)

$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''InfoReq)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''InfoRes)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''InitialStateReq)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''InitialStateRes)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''PerformActionReq)
$(deriveJSONAndTypeScript (defaultOptions {sumEncoding = TwoElemArray}) ''PerformActionRes)
