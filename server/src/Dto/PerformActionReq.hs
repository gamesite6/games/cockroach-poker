module Dto.PerformActionReq where

import Shared.Action (Action)
import Shared.GameState (GameState)
import Shared.PlayerId (PlayerId)

data PerformActionReq = PerformActionReq
  { performedBy :: PlayerId,
    action :: Action,
    state :: GameState,
    seed :: Int
  }
  deriving (Eq, Show)
