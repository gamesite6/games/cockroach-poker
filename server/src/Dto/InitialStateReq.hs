module Dto.InitialStateReq where

import Shared.PlayerId
import Shared.Settings (GameSettings)

data InitialStateReq = InitialStateReq
  { players :: [PlayerId],
    settings :: GameSettings,
    seed :: Int
  }
  deriving (Eq, Show)
