module InitialState (initialState) where

import Control.Monad.Random (evalRand)
import Control.Monad.Random.Class
import Control.Monad.Trans.Maybe
import Data.List
import Data.List.Split (chunksOf)
import Shared.GameState (GameState (GameState))
import qualified Shared.GameState as GameState
import Shared.GameState.Card
import Shared.GameState.Phase
import Shared.GameState.Phase.Giving
import Shared.GameState.Player (Player (Player))
import qualified Shared.GameState.Player as Player
import Shared.PlayerId
import Shared.Settings (GameSettings, Seed (..))
import System.Random (mkStdGen)
import System.Random.Shuffle (shuffleM)

initialState :: Seed -> GameSettings -> [PlayerId] -> Maybe GameState
initialState seed settings playerIds =
  let gen = mkStdGen (unSeed seed)
   in evalRand (runMaybeT $ initialStateM settings playerIds) gen

_nothing :: Monad m => MaybeT m a
_nothing = MaybeT (pure Nothing)

divideCards :: Int -> [Card] -> [[Card]]
divideCards playerCount cards =
  let hands = replicate playerCount []

      divideChunk :: [[Card]] -> [Card] -> [[Card]]
      divideChunk (h : hs) (c : cs) = (c : h) : divideChunk hs cs
      divideChunk (h : hs) [] = h : divideChunk hs []
      divideChunk [] _ = []

      cardChunks = chunksOf playerCount cards
   in foldl' divideChunk hands cardChunks

initialStateM :: MonadRandom m => GameSettings -> [PlayerId] -> MaybeT m GameState
initialStateM _settings playerIds =
  let playerCount = length playerIds
   in do
        cards <-
          if playerCount == 2
            then drop 10 <$> shuffleM allCards
            else shuffleM allCards
        let hands = divideCards playerCount cards
            players =
              zipWith
                ( \playerId hand ->
                    Player
                      { Player.id = playerId,
                        Player.hand = hand,
                        Player.palmed = []
                      }
                )
                playerIds
                hands
        pure $
          GameState
            { GameState.players = players,
              GameState.phase = Giving $ GivingData (head playerIds)
            }
