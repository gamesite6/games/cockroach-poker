{-# LANGUAGE NamedFieldPuns #-}

module PerformAction (performAction) where

import Control.Monad.Random (Rand, evalRand)
import Control.Monad.State
import Control.Monad.Trans.Maybe
import Data.List (find, group, sort)
import Data.Maybe (fromMaybe)
import Data.Set (Set)
import qualified Data.Set as Set
import Shared.Action
import Shared.Action.Accept as Accept
import Shared.Action.Give as Give
import Shared.Action.Pass as Pass
import Shared.GameState (GameState (..))
import qualified Shared.GameState as GameState
import Shared.GameState.Card as Card
import Shared.GameState.Phase
import Shared.GameState.Phase.Acceptance as Acceptance
import Shared.GameState.Phase.Claim as Claim
import Shared.GameState.Phase.GameComplete as GameComplete
import Shared.GameState.Phase.Giving as Giving
import Shared.GameState.Phase.Passing as Passing
import Shared.GameState.Phase.Receiving as Receiving
import Shared.GameState.Player (Player (Player))
import qualified Shared.GameState.Player as Player
import Shared.PlayerId
import Shared.Settings (Seed (..))
import System.Random (StdGen, mkStdGen)

performAction :: Seed -> PlayerId -> Action -> GameState -> Maybe GameState
performAction seed userId action gameState =
  let gen = mkStdGen (unSeed seed)
   in evalRand (evalStateT (runMaybeT (performActionT userId action)) gameState) gen

type ActionM a = MaybeT (StateT GameState (Rand StdGen)) a

nothing :: ActionM a
nothing = liftMaybe Nothing

liftMaybe :: Maybe a -> ActionM a
liftMaybe = MaybeT . pure

isActionAllowed :: PlayerId -> Action -> GameState -> Bool
isActionAllowed userId action GameState {players, phase} =
  fromMaybe False $ do
    user <- find ((== userId) . Player.id) players
    let Player {Player.hand = userHand} = user
    case phase of
      Giving GivingData {Giving.giver = giverId} ->
        pure $
          userId == giverId && case action of
            Give
              GiveData
                { Give.receiver = receiverId,
                  Give.actual = actualCard
                } -> userId /= receiverId && elem actualCard userHand
            _ -> False
      Receiving
        ReceivingData
          { Receiving.claim = currentClaim@Claim {Claim.receiver = receiverId},
            Receiving.previous = previousClaims
          } ->
          let allClaims = currentClaim : previousClaims
              hasSeenCard :: Set PlayerId
              hasSeenCard = Set.fromList $ userId : (Claim.giver <$> allClaims)
           in pure $
                userId == receiverId && case action of
                  Peek -> any (`Set.notMember` hasSeenCard) (Player.id <$> players)
                  Accept _ -> True
                  _ -> False
      Acceptance
        AcceptanceData
          { Acceptance.claim =
              Claim
                { Claim.giver = giverId,
                  Claim.receiver = receiverId
                },
            Acceptance.ready = ready
          } -> pure $ (userId == giverId || userId == receiverId) && userId `Set.notMember` ready
      Passing
        PassingData
          { Passing.giver = giverId,
            Passing.claims = previousClaims
          } ->
          let hasSeenCard :: Set PlayerId
              hasSeenCard = Set.fromList $ Claim.giver <$> previousClaims
           in pure $
                userId == giverId && case action of
                  Pass PassData {Pass.receiver = receiverId} -> receiverId `Set.notMember` hasSeenCard
                  _ -> False
      GameComplete _ -> pure False

removeCardFromHand :: Card -> GameState -> GameState
removeCardFromHand card s@GameState {players} =
  let removeCard :: Card -> Player -> Player
      removeCard c p@Player {Player.hand = hand} = p {Player.hand = filter (/= c) hand}
   in s
        { GameState.players = map (removeCard card) players
        }

palmOffCard :: Card -> PlayerId -> GameState -> GameState
palmOffCard card playerId s@GameState {players} =
  s {GameState.players = map palm players}
  where
    palm :: Player -> Player
    palm p@Player {Player.palmed = palmed} =
      if Player.id p == playerId
        then p {Player.palmed = card : palmed}
        else p

performActionT :: PlayerId -> Action -> ActionM GameState
performActionT userId action = do
  previousState <- get
  let GameState {GameState.phase = previousPhase} = previousState
  _ <-
    if isActionAllowed userId action previousState
      then pure ()
      else nothing
  _ <- case previousPhase of
    Giving GivingData {} -> case action of
      Give
        GiveData
          { Give.claimed = claimedSuit,
            Give.actual = actualCard,
            Give.receiver = receiverId
          } -> do
          modify (removeCardFromHand actualCard)
          modify $
            setPhase $
              Receiving
                ReceivingData
                  { Receiving.claim =
                      Claim
                        { Claim.suit = claimedSuit,
                          Claim.giver = userId,
                          Claim.receiver = receiverId
                        },
                    Receiving.actual = actualCard,
                    Receiving.previous = []
                  }
      _ -> nothing
    Receiving
      ReceivingData
        { Receiving.actual = actualCard,
          Receiving.claim = currentClaim,
          Receiving.previous = previousClaims
        } -> case action of
        Peek ->
          modify $
            setPhase $
              Passing
                PassingData
                  { Passing.giver = userId,
                    Passing.card = actualCard,
                    Passing.claims = currentClaim : previousClaims
                  }
        Accept AcceptData {Accept.belief = belief} ->
          modify $
            setPhase $
              Acceptance
                AcceptanceData
                  { Acceptance.claim = currentClaim,
                    Acceptance.belief = belief,
                    Acceptance.ready = Set.empty,
                    Acceptance.actual = actualCard
                  }
        _ -> nothing
    Acceptance
      phaseData@AcceptanceData
        { Acceptance.claim =
            Claim
              { Claim.suit = claimedSuit,
                Claim.giver = giverId,
                Claim.receiver = receiverId
              },
          Acceptance.belief = belief,
          Acceptance.ready = ready,
          Acceptance.actual = actualCard
        } -> case action of
        Ready ->
          let actualSuit = Card.cardSuit actualCard
              truth = actualSuit == claimedSuit
              loserId = if truth == belief then giverId else receiverId
              nextReady = Set.insert userId ready
              allReady = Set.size nextReady == 2
           in if allReady
                then do
                  modify $ palmOffCard actualCard loserId
                  loser <- MaybeT $ gets (find ((== loserId) . Player.id) . GameState.players)
                  let Player {Player.palmed = loserPalmed, Player.hand = loserHand} = loser
                      identicalRequiredToLose = if (length . players) previousState == 2 then 5 else 4
                      groupedBySuit = group (sort (cardSuit <$> loserPalmed))
                      loserHas4Identical = any (\g -> length g >= identicalRequiredToLose) groupedBySuit
                      loserHasEmptyHand = null loserHand
                  if loserHas4Identical || loserHasEmptyHand
                    then modify $ setPhase $ GameComplete GameCompleteData {GameComplete.loser = loserId}
                    else modify $ setPhase $ Giving GivingData {Giving.giver = loserId}
                else modify $ setPhase $ Acceptance phaseData {Acceptance.ready = nextReady}
        _ -> nothing
    Passing
      PassingData
        { Passing.card = actualCard,
          Passing.claims = previousClaims
        } -> case action of
        Pass
          PassData
            { Pass.claimed = claimed,
              Pass.receiver = receiver
            } ->
            let claim =
                  Claim
                    { Claim.suit = claimed,
                      Claim.giver = userId,
                      Claim.receiver = receiver
                    }
             in modify $
                  setPhase $
                    Receiving
                      ReceivingData
                        { Receiving.previous = previousClaims,
                          Receiving.claim = claim,
                          Receiving.actual = actualCard
                        }
        _ -> nothing
    GameComplete _ -> nothing
  get

setPhase :: Phase -> GameState -> GameState
setPhase phase s = s {phase}