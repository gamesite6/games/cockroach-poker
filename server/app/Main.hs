{-# LANGUAGE DataKinds #-}
{-# LANGUAGE NamedFieldPuns #-}
{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE TypeOperators #-}

module Main where

import Dto.InfoReq (InfoReq (InfoReq))
import qualified Dto.InfoReq as InfoReq
import Dto.InfoRes
import Dto.InitialStateReq
import Dto.InitialStateRes (InitialStateRes (InitialStateRes))
import Dto.Json ()
import Dto.PerformActionReq
import Dto.PerformActionRes
import qualified Lib
import Network.Wai
import Network.Wai.Handler.Warp
import Servant
import Shared.Settings (Seed (..))
import System.Environment (getEnv)

type GameAPI =
  "info" :> ReqBody '[JSON] InfoReq :> Post '[JSON] InfoRes
    :<|> "initial-state" :> ReqBody '[JSON] InitialStateReq :> Post '[JSON] InitialStateRes
    :<|> "perform-action" :> ReqBody '[JSON] PerformActionReq :> Post '[JSON] PerformActionRes

server :: Server GameAPI
server = handleInfo :<|> handleInitialState :<|> handlePerformAction

main :: IO ()
main = do
  portStr <- getEnv "PORT"
  let port = (read portStr) :: Int
  putStrLn $ "Listening on port " <> (show port)
  run port app

handleInfo :: InfoReq -> Handler InfoRes
handleInfo InfoReq {InfoReq.settings = settings} =
  let playerCounts = Lib.validatePlayerCounts settings
   in pure $ InfoRes playerCounts

handleInitialState :: InitialStateReq -> Handler InitialStateRes
handleInitialState req@InitialStateReq {players, settings} =
  let seed = Seed $ Dto.InitialStateReq.seed req
   in case Lib.initialState seed settings players of
        Just state -> pure $ InitialStateRes state
        Nothing -> throwError $ err422

handlePerformAction :: PerformActionReq -> Handler PerformActionRes
handlePerformAction req@PerformActionReq {performedBy, action, state} =
  let seed = Seed $ Dto.PerformActionReq.seed req
   in case Lib.performAction seed performedBy action state of
        Just nextState ->
          pure
            PerformActionRes
              { nextState,
                completed = Lib.isCompleted nextState
              }
        Nothing -> throwError $ err422

gameAPI :: Proxy GameAPI
gameAPI = Proxy

app :: Application
app = serve gameAPI server
