module Main where

import Data.Aeson.TypeScript.TH
import Data.Monoid ()
import Data.Proxy
import Dto.Json ()
import Shared.Action
import Shared.Action.Accept
import Shared.Action.Give
import Shared.Action.Pass
import Shared.GameState
import Shared.GameState.Card
import Shared.GameState.Phase
import Shared.GameState.Phase.Acceptance
import Shared.GameState.Phase.Claim
import Shared.GameState.Phase.GameComplete
import Shared.GameState.Phase.Giving
import Shared.GameState.Phase.Passing
import Shared.GameState.Phase.Receiving
import Shared.GameState.Player
import Shared.PlayerId
import Shared.Settings

main :: IO ()
main =
  putStrLn $
    formatTSDeclarations
      ( (getTypeScriptDeclarations (Proxy :: Proxy PlayerId))
          <> (getTypeScriptDeclarations (Proxy :: Proxy GameSettings))
          <> (getTypeScriptDeclarations (Proxy :: Proxy GameState))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Player))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Card))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Suit))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Action))
          <> (getTypeScriptDeclarations (Proxy :: Proxy GiveData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy PassData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy AcceptData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Phase))
          <> (getTypeScriptDeclarations (Proxy :: Proxy GivingData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy ReceivingData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy AcceptanceData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy PassingData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy GameCompleteData))
          <> (getTypeScriptDeclarations (Proxy :: Proxy Claim))
      )